import serial
import time
import re

dev_name = '/dev/ttyACM0'
scoop_size = 0x80
skip_part = ["dtb1"]

def xmit(data, xtimeout=0.1):
	response = b''
	try:
		with serial.Serial(dev_name, timeout=xtimeout) as ser:
			ser.write(data.encode())
			while True:
				response_tmp = ser.read(1024)
				response += response_tmp
				if not len(response_tmp) == 1024:
					break
	except (OSError, FileNotFoundError, serial.serialutil.SerialException) as e:
		print('IO exception')
		time.sleep(2)
	return response.decode('utf-8')

def send_at(at_cmd):
	at_cmd += '\r'
	return xmit(at_cmd, xtimeout=1.0)

def get_mtd_table():
	response = xmit('mtd\r')
	start = response.find('#:')
	if start < 0:
		return ''
	line_idx = 0
	output = []
	for line in response[start:].split('\n\r'):
		if len(line) < 2:
			break
		if line.find('#') >= 0:
			continue
		rows = line.split()
		row_idx = 0
		row_dict ={}
		row_names = ["idx", "name", "size", "offset", "flags"]
		for row in rows:
			if row_idx == 0:
				row = re.sub('\:$', '', row)
			row_dict[row_names[row_idx]] = row
			row_idx += 1
		output.append(row_dict)
		line_idx += 1
	return output

def dump_part(record):
	part_size = int(record['size'], 16)
	name = record['name']
	response = xmit('nand read ${loadaddr} ' + name + '\r')
	print(response)
	response = xmit('md.l ${loadaddr} ' + hex(scoop_size) + '\r')
	start_part_addr = -1
	start_line_addr = 0
	linear_addr = 0
	run = True
	while run:
		for line in response.split('\n\r'):
			if not run:
				break
			if line.find(':') < 0:
				continue
			rows = line.split()
			start_line_addr = int(re.sub('\:$', '', rows[0]), 16)
			if start_part_addr < 0:
				start_part_addr = start_line_addr
			if start_line_addr != linear_addr + start_part_addr:
				print('linear_addr {} != start_line_addr {}'.format(linear_addr + start_part_addr, start_line_addr))
			print(start_part_addr + part_size, start_line_addr, rows)
			for i in range(1,5):
				fd.write(int(rows[i], 16).to_bytes(4, byteorder='big', signed=False))
				linear_addr += 4
				if linear_addr >= part_size:
					run = False
					break
		print('linear_addr {}, part_size {}'.format(linear_addr, part_size))
		if linear_addr >= part_size:
			run = False
			break
		else:
			response = xmit('\r')
	xmit(' \r')

for n in range(4):
	response = send_at('AT')
	if len(response) > 0:
		break
	else:
		time.sleep(4)
send_at('AT')
response = send_at('AT+CFUN?')
if response.find('+CFUN:') >= 0:
	print('in AT mode')
	send_at('AT+CFUN=1,1')
	time.sleep(1)
	response = xmit(' \r\n')
	while response.find('#') < 0:
		time.sleep(1)
		response = xmit(' \r')
if response.find('#') >= 0:
	print('in U-Boot')
	xmit(' \r')
	mtd_table = get_mtd_table()
	for record in mtd_table:
		if record['name'] in skip_part:
			print('skipping part: {}'.format(record['name']))
			continue
		dump_part(record)
	xmit('run boot_default\r')
else:
	print('switching into U-Boot failed')

